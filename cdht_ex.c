#include "cdht.h"

void addMSG(msg** head,msg* pMSG,pthread_mutex_t* lock)
{
    pthread_mutex_lock(lock);
    if (*head)
    {
        msg* p = *head;
        for (; p->next != NULL; p = p->next) {
        }
        p->next = pMSG;
    }
    else
    {
        *head = pMSG;
    }
    pthread_mutex_unlock(lock);
}
msg* readMSG(msg** head,pthread_mutex_t* lock)
{
    pthread_mutex_lock(lock);
    msg* p = *head;
    *head = (*head)->next;
    pthread_mutex_unlock(lock);
    return p;
}

void changeSuccessor(Node* self,int quitp,int Succ1,int Succ2)
{
    if(self->successor1 == quitp)
    {
        self->successor1 = Succ1;
        self->successor2 = Succ2;
    }
    else
    {
        self->successor2 = Succ1;
        fprintf(stdout,"");
    }
    fprintf(stdout,"My first successor is now peer %d.\n",self->successor1 - 50000);
    fprintf(stdout,"My second successor is now peer %d.\n",self->successor2 - 50000);
}

void th_UDPListin_Fuc(void* s)
{
    int recvlen;
    Node* self = (Node*) s;
    unsigned char* buf = malloc(sizeof(unsigned char) * MAX_BUFF);
    memset(buf, 0, sizeof(unsigned char) * MAX_BUFF);
    struct sockaddr_in remaddr;
    socklen_t readdrlen = sizeof(remaddr);
    for (;;)
    {
        recvlen = recvfrom(self->pUDPINFO->fd,buf,MAX_BUFF,0,(struct sockaddr*)&remaddr,&readdrlen);

        const char* d = ":,";
        char* command = strtok((char*)buf, d);

        if (0 == strcmp("res_ping", command))
        {
                fprintf(stdout,"A ping response message was received from Peer");
                char* v = strtok(NULL, d);
                int peerport = atoi(v);
                fprintf(stdout,"%d\n",peerport - 50000);
                if(self->successor1== peerport)
                    self->s1times--;
                if(self->successor2== peerport)
                    self->s2times--;
        }
        if (0 == strcmp("ping", command))
        {
            char* v = strtok(NULL, d);
            int peerport = atoi(v);
            fprintf(stdout,"A ping request message was received from Peer");
            fprintf(stdout,"%d\n",peerport - 50000);

            msg* m = (msg*)malloc(sizeof(msg));
            memset(m,0,sizeof(msg));
            m->type = res_ping;
            strcpy(m->value,v);
            addMSG(&(self->UDPMSG),m,self->udpmsglock);
            sem_post(self->sem_ping);

            self->peer2 = self->peer1;
            self->peer1 = peerport;

        }
        memset(buf, 0, sizeof(unsigned char) * MAX_BUFF);
    }
}

void th_UDPSent_Fuc(void* s) {
    Node* self = (Node*) s;

    struct sockaddr_in servaddr; /* server address */
    memset((char*)&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    inet_pton(AF_INET, "127.0.0.1", &servaddr.sin_addr);

    for(;;)
    {
        sem_wait(self->sem_ping);
        msg* m = readMSG(&(self->UDPMSG), self->udpmsglock);
        if (m->type == ping) {
            char strMsg[MAX_BUFF];
            memset(strMsg, 0, sizeof(char) * MAX_BUFF);
            sprintf(strMsg,"ping:%d,%d,%d",self->port,self->successor1,self->successor2);

            servaddr.sin_port = htons(self->successor1);
            if (sendto(self->pUDPINFO->fd, strMsg,
                       strlen(strMsg), 0,
                       (struct sockaddr *)&servaddr,
                       sizeof(servaddr)) < 0)
            {
                perror("sendto failed");
            }
            else
            {
                self->s1times++;
            }

            servaddr.sin_port = htons(self->successor2);
            if (sendto(self->pUDPINFO->fd, strMsg,
                       strlen(strMsg), 0,
                       (struct sockaddr *)&servaddr,
                       sizeof(servaddr)) < 0)
            {
                perror("sendto failed");
            }
            else
            {
                self->s2times++;
            }

            if(self->s1times > 4)
            {

                 msg* m =(msg*) malloc(sizeof(msg));
                 memset(m,0,sizeof(msg));
                 m->type = tcp_req_secc;
                 //snprintf(m->value,sizeof(m->value),"%d",self->successor1);
                 addMSG(&(self->TCPMSG),m,self->tcpmsglock);

                 int temp =self->successor1;
                 self->successor1 = self->successor2;
                 self->successor2 = temp;

                 temp = self->s1times;
                 self->s1times = self->s2times;
                 self->s2times = temp;

                 fprintf(stdout,"Peer %d is no longer alive.\n",self->successor2 - 50000);
                 fprintf(stdout,"My first successor is now peer %d.\n",self->successor1 - 50000);
                 sem_post(self->sem_command);
            }
            else if(self->s2times > 4)
            {
                 msg* m =(msg*) malloc(sizeof(msg));
                 memset(m,0,sizeof(msg));
                 m->type = tcp_req_secc;
                 //snprintf(m->value,sizeof(m->value),"%d",self->successor2);
                 addMSG(&(self->TCPMSG),m,self->tcpmsglock);
                 fprintf(stdout,"Peer %d is no longer alive.\n",self->successor2 - 50000);
                 fprintf(stdout,"My first successor is now peer %d.\n",self->successor1 - 50000);
                 sem_post(self->sem_command);
            }
        }
        else if (m->type == res_ping)
        {
            int port = atoi(m->value);
            char msg[20];
            memset(msg, 0, sizeof(msg));
            sprintf(msg,"res_ping:%d",self->port);

            servaddr.sin_port = htons(port);
            if (sendto(self->pUDPINFO->fd, msg,
                       strlen(msg), 0,
                       (struct sockaddr *)&servaddr,
                       sizeof(servaddr)) < 0)
            {
                perror("sendto failed");
            }
        }
        free(m);
    }
}

void th_TCPListen_Fuc(void* s) {
    Node* self = (Node*) s;
    listen(self->pTCPINFO->fd,8);
    struct sockaddr_in cliAddr;
    socklen_t cliLen = sizeof(cliAddr);
    char buf[MAX_BUFF];
    char* d = " ,:\n";
    for(;;)
    {
        memset(buf,0,MAX_BUFF);
        int sd = accept(self->pTCPINFO->fd,(struct sockaddr*)&cliAddr,&cliLen);
        if(sd < 0)
            perror("ACCEPT ERROR");
        int n= recv(sd,buf,sizeof(buf)-1,0);
        if(n < 0)
            perror("RECEIVE ERROR");
        char* command = strtok(buf,d);
        if(strcmp(command,"quit") == 0)
        {
            int quitport = atoi(strtok(NULL,d));
            fprintf(stdout,"Peer %d will depart from the network.\n",quitport - 50000);
            int newsuccessor1 = atoi(strtok(NULL,d));
            int newsuccessor2 = atoi(strtok(NULL,d));
            changeSuccessor(self,quitport,newsuccessor1,newsuccessor2);
        }
        else if(strcmp(command,"request") == 0)
        {
            int file = atoi(strtok(NULL,d));
            int port = atoi(strtok(NULL,d));
            int hs = file % 256;
            int selfhs = self->port - 50000;
            int succ_hs = self->successor1 - 50000;

            if(hs < selfhs || (hs > selfhs && selfhs < succ_hs && hs >= succ_hs))
            {
                msg* pMsg = malloc(sizeof(msg));
                memset(pMsg, 0, sizeof(msg));
                pMsg->type = tcp_request;
                snprintf(pMsg->value, sizeof(pMsg->value),"request:%d:%d",file,port);
                addMSG(&(self->TCPMSG), pMsg, self->tcpmsglock);
                fprintf(stdout,"File %d is not stored here.\n",file);
                sem_post(self->sem_command);
            }else if(selfhs > succ_hs || (hs >= selfhs && hs < succ_hs))
            {
                msg* pMsg = malloc(sizeof(msg));
                memset(pMsg, 0, sizeof(msg));
                pMsg->type = tcp_response;
                snprintf(pMsg->value, sizeof(pMsg->value),"response:%d:%d",file,port);
                addMSG(&(self->TCPMSG), pMsg, self->tcpmsglock);
                fprintf(stdout,"File %d is here.\n",file);
                sem_post(self->sem_command);
            }
        }
        else if(strcmp(command,"response") == 0)
        {
            int file = atoi(strtok(NULL,d));
            int port = atoi(strtok(NULL,d)) - 50000;
            fprintf(stdout,"Received a response message from %d, which has the file %d\n",port,file);
        }
        else if(strcmp(command,"req_succ") == 0)
        {
            memset(buf,0,MAX_BUFF);
            snprintf(buf,sizeof(buf),"%d:%d",self->successor1,self->successor2);
            send(sd,buf,sizeof(buf),0);
        }

        close(sd);
    }

}

void th_TCPSent_Fuc(void* s)
{
    Node* self = (Node*) s;
    struct sockaddr_in localAddr,serAddr;
    serAddr.sin_family = AF_INET;
    inet_pton(AF_INET, "127.0.0.1", &serAddr.sin_addr);

    for(;;)
    {
        int rc = 0;
        int sd = socket(AF_INET,SOCK_STREAM,0);
        localAddr.sin_family = AF_INET;
        localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
        localAddr.sin_port = htons(0);
        rc = bind(sd, (struct sockaddr *) &localAddr, sizeof(localAddr));
        if(rc < 0)
            perror("BIND Error");

        sem_wait(self->sem_command);
        msg* m = readMSG(&(self->TCPMSG),self->tcpmsglock);
        char* c = malloc(sizeof(char) * 50);
        memset(c,0,50);
        strcpy(c,m->value);
        if(m->type == tcp_request)
        {
            serAddr.sin_port = htons(self->successor1);
            strtok(c,":");
            rc = connect(sd,(struct sockaddr*)&serAddr,sizeof(serAddr));
            if(rc < 0)
                perror("CONNECT Error");
            rc = send(sd,m->value,strlen(m->value) + 1,0);
            if(rc < 0)
                perror("SEND Error");
            int file = atoi(strtok(NULL,":"));
            int port = atoi(strtok(NULL,":"));
            if (port == self->port)
                fprintf(stdout,"File request message for %d has been sent to my successor\n",file);
            else
                fprintf(stdout,"File request message has been sent to my successor\n");

        }
        else if(m->type == tcp_response)
        {
            strtok(c,":");
            int file = atoi(strtok(NULL,":"));
            int port = atoi(strtok(NULL,":"));
            serAddr.sin_port = htons(port);
            connect(sd,(struct sockaddr*)&serAddr,sizeof(serAddr));
            char strMsg[30];
            memset(strMsg,0,30);
            snprintf(strMsg,sizeof(strMsg),"response:%d:%d",file,self->port);
            send(sd,strMsg,sizeof(strMsg),0);
            fprintf(stdout,"A response message, destined for peer %d, has been sent\n",port - 50000);
        }
        else if(m->type == tcp_quit)
        {
            //give them two succ choice
            //quit:port:succ1:succ2

            char strMsg[50];
            memset(strMsg,0,50);
            snprintf(strMsg,sizeof(strMsg),"quit:%d:%d:%d",self->port,self->successor1,self->successor2);

            int sd1 = socket(AF_INET,SOCK_STREAM,0);
            rc = bind(sd1, (struct sockaddr *) &localAddr, sizeof(localAddr));
            serAddr.sin_port = htons(self->peer1);
            //send to first peer
            rc = connect(sd1,(struct sockaddr*)&serAddr,sizeof(serAddr));
            if(rc < 0)
                perror("CONNECT Error");
            rc = send(sd1,strMsg,strlen(strMsg) + 1,0);
            if(rc < 0)
                perror("SEND Error");

            //send to second peer
            serAddr.sin_port = htons(self->peer2);
            rc = connect(sd,(struct sockaddr*)&serAddr,sizeof(serAddr));
            if(rc < 0)
                perror("CONNECT Error");
            rc = send(sd,strMsg,strlen(strMsg) + 1,0);
            if(rc < 0)
                perror("SEND Error");
            close(sd1);

            sem_post(self->sem_cmdq);
        }
        else if(m->type == tcp_req_secc)
        {
            serAddr.sin_port = htons(self->successor1);
            connect(sd,(struct sockaddr*)&serAddr,sizeof(serAddr));
            char strMsg[30];
            memset(strMsg,0,30);
            snprintf(strMsg,sizeof(strMsg),"req_succ:");
            send(sd,strMsg,sizeof(strMsg),0);

            memset(strMsg,0,30);
            recv(sd,strMsg,sizeof(strMsg),0);
            int succ1 = atoi(strtok(strMsg,":"));
            int succ2 = atoi(strtok(NULL,":"));
            int deletsucc = self->successor2;
            if (deletsucc == succ1)
                self->successor2 = succ2;
            else
                self->successor2 = succ1;

            fprintf(stdout,"My second successor is now peer %d.\n",self->successor2 - 50000);
            self->s2times = 0;
        }

        close(sd);
        free(m);
    }

}

void th_Command_Fuc(void* v) {
    Node* self = (Node*) v;
    char buf[MAX_BUFF];
    memset(buf, 0, sizeof(char) * MAX_BUFF);
    const char* d= " \n";
    while (1) {
        fgets(buf, MAX_BUFF, stdin);
        char* command = strtok(buf, d);
        if (0==strcmp("quit", command)) {
            msg* pMsg = malloc(sizeof(msg));
            memset(pMsg, 0, sizeof(msg));
            pMsg->type = tcp_quit;
            addMSG(&(self->TCPMSG), pMsg, self->tcpmsglock);
            sem_post(self->sem_command);
        }else if (0==strcmp("request", command)) {
            char* v = strtok(NULL, d);

            msg* pMsg = malloc(sizeof(msg));
            memset(pMsg, 0, sizeof(msg));
            pMsg->type = tcp_request;
            snprintf(pMsg->value,sizeof(pMsg->value),"request:%d:%d",atoi(v),self->port);
            addMSG(&(self->TCPMSG), pMsg, self->tcpmsglock);

            sem_post(self->sem_command);
        }else if (0==strcmp("silencequit", command)) {
            //ONLY USE FOR DEBUG
            sem_post(self->sem_cmdq);
        }
        memset(buf, 0, sizeof(char) * MAX_BUFF);
    }
}

void th_TimerFuc(void* s) {
    sleep(1);
    Node* self = (Node*) s;
    for(;;)
    {
        msg* pMsg = malloc(sizeof(msg));
        memset(pMsg, 0, sizeof(msg));
        pMsg->type = ping;
        addMSG(&(self->UDPMSG), pMsg, self->udpmsglock);
        sem_post(self->sem_ping);
        sleep(5);
    }
}

Node* init(const char** argv) {
    //initialize self infor node
    Node* self = (Node*)malloc(sizeof(Node));
    memset(self,0,sizeof(Node));
    self->pUDPINFO = (ConnectInfo*)malloc(sizeof(ConnectInfo));
    memset(self->pUDPINFO,0,sizeof(ConnectInfo));
    self->pTCPINFO = (ConnectInfo*)malloc(sizeof(ConnectInfo));
    memset(self->pTCPINFO,0,sizeof(ConnectInfo));

    self->port = 50000 + atoi(argv[1]);
    self->successor1= 50000 + atoi(argv[2]);
    self->successor2= 50000 + atoi(argv[3]);

    //initialize socket
    if ((self->pUDPINFO->fd = socket(AF_INET,SOCK_DGRAM,0)) < 0) {
        perror("cannot create UDP socket");
    }

    if ((self->pTCPINFO->fd = socket(AF_INET,SOCK_STREAM,0)) < 0) {
        perror("cannot create TCP socket");
    }

    self->pUDPINFO->myaddr.sin_family = AF_INET;
    self->pUDPINFO->myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    self->pUDPINFO->myaddr.sin_port = htons(self->port);
    if (bind(self->pUDPINFO->fd,
             (struct sockaddr*)&self->pUDPINFO->myaddr ,
             sizeof(self->pUDPINFO->myaddr)))
    {
        perror("bind UDP error");
    }

    self->pTCPINFO->myaddr.sin_family = AF_INET;
    self->pTCPINFO->myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    self->pTCPINFO->myaddr.sin_port = htons(self->port);
    if (bind(self->pTCPINFO->fd,
             (struct sockaddr*)&self->pTCPINFO->myaddr ,
             sizeof(self->pTCPINFO->myaddr)))
    {
        perror("bind TCP error");
    }

    //initialize thread lock
    self->udpmsglock = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
    self->tcpmsglock = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));

    pthread_mutex_init(self->udpmsglock, NULL);
    pthread_mutex_init(self->tcpmsglock, NULL);

    //initialize semaphore
    int cnt = 0;
    snprintf(self->sem_pname, sizeof(self->sem_pname),"%d,%d",(int)getpid(),cnt++);
    self->sem_ping = sem_open(self->sem_pname, O_CREAT|O_EXCL,S_IRWXU,0);
    if (self->sem_ping == SEM_FAILED) {
        perror("FAILED");
    }

    snprintf(self->sem_cname, sizeof(self->sem_cname),"%d,%d",(int)getpid(),cnt++);
    self->sem_command = sem_open(self->sem_cname, O_CREAT|O_EXCL,S_IRWXU,0);
    if (self->sem_command== SEM_FAILED) {
        perror("FAILED");
    }

    snprintf(self->sem_cmdqname, sizeof(self->sem_cmdqname),"%d,%d",(int)getpid(),cnt++);
    self->sem_cmdq = sem_open(self->sem_cmdqname, O_CREAT|O_EXCL,S_IRWXU,0);
    if (self->sem_cmdq== SEM_FAILED) {
        perror("FAILED");
    }

    return self;
};

void destory(Node* self)
{
    close(self->pUDPINFO->fd);
    close(self->pTCPINFO->fd);
    free(self->pUDPINFO);
    free(self->pTCPINFO);

    pthread_mutex_destroy(self->udpmsglock);
    pthread_mutex_destroy(self->tcpmsglock);


    sem_unlink(self->sem_pname);
    sem_unlink(self->sem_cname);
    sem_unlink(self->sem_cmdqname);
    free(self);
}

int main(int argc, const char * argv[]) {

   Node* self = init(argv);

   pthread_create(&th_UDPSent, NULL, (void*)th_UDPSent_Fuc, self);
   pthread_create(&th_UDPListen, NULL, (void*)th_UDPListin_Fuc, self);
   pthread_create(&th_Timer, NULL, (void*)th_TimerFuc, self);
   pthread_create(&th_Command, NULL, (void*)th_Command_Fuc, self);
   pthread_create(&th_TCPListen,NULL,(void*)th_TCPListen_Fuc,self);
   pthread_create(&th_TCPSent,NULL,(void*)th_TCPSent_Fuc,self);

   sem_wait(self->sem_cmdq);

   pthread_cancel(th_Timer);
   pthread_cancel(th_UDPSent);
   pthread_cancel(th_Command);
   pthread_cancel(th_TCPListen);
   pthread_cancel(th_TCPSent);
   pthread_cancel(th_UDPListen);

   destory(self);

   return 0;
}

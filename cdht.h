#ifndef __CDHT_H__
#define __CDHT_H__

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/socket.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <errno.h>

#define TRUE 1
#define FALSE 0
#define MAX_BUFF 255

//struct declaration and difination
struct _Node;
struct _ConnectInfo;
struct _msg;


pthread_t th_UDPListen,th_UDPSent,th_Command,th_Timer,th_TCPSent,th_TCPListen;

typedef enum _msgtype {
    ping,
    res_ping,
    tcp_request,
    tcp_response,
    tcp_quit,
    tcp_req_secc,
} msgtype;

typedef struct _msg {
    msgtype type;
    char value[50];
    struct _msg* next;
}msg;

typedef struct _Node {
    unsigned int port;
    struct _ConnectInfo* pUDPINFO;
    struct _ConnectInfo* pTCPINFO;
    unsigned int successor1;
    unsigned int successor2;
    short s1times;
    short s2times;

    unsigned int peer1;
    unsigned int peer2;

    sem_t* sem_ping;
    char sem_pname[10];

    sem_t* sem_command;
    char sem_cname[10];

    sem_t* sem_cmdq;
    char sem_cmdqname[10];

    msg* UDPMSG;
    msg* TCPMSG;
    pthread_mutex_t* udpmsglock;
    pthread_mutex_t* tcpmsglock;
} Node;

typedef struct _ConnectInfo {
    int fd;
    struct sockaddr_in myaddr;
} ConnectInfo;

//function declaration
Node* init(const char** argv);
void destory(Node* self);

void addMSG(msg** head,msg* pMSG,pthread_mutex_t* lock);
msg* readMSG(msg** head,pthread_mutex_t* lock);
void th_UDPListin_Fuc(void* v) ;
void th_UDPSent_Fuc(void* v) ;
void th_TCPListen_Fuc(void* s) ;
void th_TCPSent_Fuc(void* s);
void th_Command_Fuc(void* v) ;
void changeSuccessor(Node* self,int quitp,int Succ1,int Succ2);

#endif
